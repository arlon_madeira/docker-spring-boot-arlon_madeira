FROM openjdk:12
ADD target/docker-spring-boot-arlon_madeira.jar docker-spring-boot-arlon_madeira.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-arlon_madeira.jar"]
